# Proposed Dockerizing of Developer Machines #

Warning: Work in Progress! Fix accordingly.

Strictly a proposal. Please weigh in.

### Proposed setup for using Docker in projects ###

* Everything is text. Everything is tracked via Git.
* Initial usage is for developer machines.

The basic idea is to lay it out as so:

    /vinsuite-admin-dev
    ├── Dockerfile  <-- instructions for building a lucee container
    ├── docker-compose.yml  <-- fit this docker image into a virtual cluster of "servers"
    ├── .dockerignore
    ├── code  <--git submodule, pointing to vinsuite's git repo, and mounted as a volume to the container
    ├── config
    │   └── lucee
    │       └── lucee-web.xml.cfm
    │   └── httpd
    │       └── conf.d
    │           └── *.conf
    ├── data  <-- .sql files of test data to load into postgresql 
    └── logs  <-- mounted to container for easy log access
    │ 	├── lucee
    │ 	├── httpd
    │ 	├── tomcat
    │ 	└── redis
    └── README.md


### Getting Started ###

* Install [Docker](https://www.docker.com/) on OSX
* Clone this git repo
* cd to repo root, and ensure you see a `Dockerfile` there
* in bash/zsh, run: `docker build`. Docker will see the `Dockerfile` automatically and use that to build a docker image.
* Run the newly built docker image (this is called, booting/starting a container): `docker run -p 8080:8080 --rm --name vinsuite-admin -it --rm -v "$PWD":/srv/app -w /srv/app lucee /bin/bash`

Explanation of the above `docker run` arguments:

* `-it`  Open a shell within the currently open OSX terminal. 
* `--rm`  Remove the container when its shut down so we don't have orphaned containers.
* `-v`  Mount a local directory into the container. One can edit the code on your OSX box and have it used by the container, same as in the current VirtualBox setups.
* `-w`  Set the current working directory when the shell starts.
* `lucee`  The name of the image to run.
* `/bin/bash`  Which shell do you want it to run?

### Future Expansion ###

* Automated spin-up of QA environments
* Play a part in automated deployments to AWS or any other supporting hosting service.

For example, a developer could start two docker containers with `docker-compose up`:

* vinsuite-api = the Java API (?)
* vinsuite-admin-browserapp = the JS single-page app (?)